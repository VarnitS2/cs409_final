const Category = require("../../models/category");

module.exports = function (router) {
  var postCategoryRoute = router.route("/category");

  postCategoryRoute.post(async function (req, res) {
    const data = new Category({
      color: req.body.color,
      title: req.body.title,
      image: req.body.image,
      user_id: req.body.user_id,
    });
    try {
      const dataToSave = await data.save();
      res.status(201).json({
        message: "201 OK",
        data: dataToSave,
      });
    } catch (error) {
      res
        .status(500)
        .json({ message: "500 SERVER ERROR", data: error.message });
    }
  });
  return router;
};
