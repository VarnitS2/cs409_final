const Category = require("../../models/category");

module.exports = function (router) {
  var getAllCategoryRoutes = router.route("/categories");

  getAllCategoryRoutes.get(async function (req, res) {
    let { where, sort, select, skip, limit, count } = req.query;
    where = where ? JSON.parse(where) : {};
    sort = sort ? JSON.parse(sort) : {};
    select = select ? JSON.parse(select) : {};
    skip = skip ? Number(skip) : 0;
    limit = limit ? Number(limit) : null;

    try {
      const data = count
        ? await Category.find(where, select)
            .sort(sort)
            .skip(skip)
            .limit(limit)
            .count()
        : await Category.find(where, select).sort(sort).skip(skip).limit(limit);
      res.status(200).json({
        message: "200 OK",
        data: data,
      });
    } catch (error) {
      res.status(500).json({
        message: "500 SERVER ERROR",
        data: error.message,
      });
    }
  });

  return router;
};
