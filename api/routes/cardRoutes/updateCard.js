const Card = require("../../models/card");

module.exports = function (router) {
  var updateCardRoute = router.route("/card/:id");

  updateCardRoute.put(async function (req, res) {
    try {
      const existingCard = await Card.findById(req.params.id);
      if (existingCard == null) {
        res.status(400).json({
          message: "404 NOT FOUND",
          data: "Card ID doesn't exist",
        });
      } else {
        const updatedCard = await Card.updateOne(
          { _id: req.params.id },
          req.body
        );
        res.status(200).json({
          message: "200 OK",
          data: updatedCard,
        });
      }
    } catch (error) {
      res
        .status(500)
        .json({ message: "500 SERVER ERROR", data: error.message });
    }
  });
  return router;
};
