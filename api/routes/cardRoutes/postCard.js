const Card = require("../../models/card");

module.exports = function (router) {
  var postCardRoute = router.route("/card");

  postCardRoute.post(async function (req, res) {
    const data = new Card({
      front: req.body.front,
      back: req.body.back,
      study_bubble_id: req.body.study_bubble_id,
    });
    try {
      const dataToSave = await data.save();
      res.status(201).json({
        message: "201 OK",
        data: dataToSave,
      });
    } catch (error) {
      res
        .status(500)
        .json({ message: "500 SERVER ERROR", data: error.message });
    }
  });
  return router;
};
