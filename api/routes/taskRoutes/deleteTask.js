const Task = require("../../models/task");
const User = require("../../models/user");

module.exports = function (router) {
  var deleteTaskRoute = router.route("/task/:id");

  deleteTaskRoute.delete(async function (req, res) {
    try {
      const taskToDelete = await Task.findById(req.params.id);
      // If the task is pending we need to clean up on the User's side
      if (taskToDelete == null) {
        res
          .status(404)
          .json({ message: "404 NOT FOUND", data: "Couldn't find this Task" });
      } else {
        const deletedTask = await Task.deleteOne({ _id: req.params.id });
        res.status(200).json({
          message: "200 OK",
          data: taskToDelete,
        });
      }
    } catch (error) {
      res
        .status(500)
        .json({ message: "500 SERVER ERROR", data: error.message });
    }
  });
  return router;
};
