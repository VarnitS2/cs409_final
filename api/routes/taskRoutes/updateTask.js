const Task = require("../../models/task");

module.exports = function (router) {
  var updateTaskRoute = router.route("/task/:id");

  updateTaskRoute.put(async function (req, res) {
    try {
      const existingTask = await Task.findById(req.params.id);
      if (existingTask == null) {
        res.status(400).json({
          message: "404 NOT FOUND",
          data: "Task ID doesn't exist",
        });
      } else {
        const updatedTask = await Task.updateOne(
          { _id: req.params.id },
          req.body
        );
        res.status(200).json({
          message: "200 OK",
          data: updatedTask,
        });
      }
    } catch (error) {
      res
        .status(500)
        .json({ message: "500 SERVER ERROR", data: error.message });
    }
  });
  return router;
};
