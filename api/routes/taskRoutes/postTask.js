const Task = require("../../models/task");

module.exports = function (router) {
  var postTaskRoute = router.route("/task");

  postTaskRoute.post(async function (req, res) {
    const data = new Task({
      description: req.body.description,
      is_complete: req.body.is_complete,
      study_bubble_id: req.body.study_bubble_id,
    });
    try {
      const dataToSave = await data.save();
      res.status(201).json({
        message: "201 OK",
        data: dataToSave,
      });
    } catch (error) {
      res
        .status(500)
        .json({ message: "500 SERVER ERROR", data: error.message });
    }
  });
  return router;
};
