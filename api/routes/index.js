/*
 * Connect all of your endpoints together here.
 */

module.exports = function (app, router) {
  app.use("/api", require("./home.js")(router));

  // Card Routes
  app.get("/api/cards", require("./cardRoutes/getAllCards.js")(router));
  app.post("/api/card", require("./cardRoutes/postCard.js")(router));
  app.put("/api/card/:id", require("./cardRoutes/updateCard.js")(router));

  // Category Routes
  app.get(
    "/api/categories",
    require("./categoryRoutes/getAllCategories.js")(router)
  );
  app.post(
    "/api/category",
    require("./categoryRoutes/postCategory.js")(router)
  );

  // Study Bubble Routes
  app.get(
    "/api/studybubbles",
    require("./studyBubbleRoutes/getAllStudyBubbles.js")(router)
  );
  app.post(
    "/api/studybubble",
    require("./studyBubbleRoutes/postStudyBubble.js")(router)
  );
  app.put(
    "/api/studybubble/:id",
    require("./studyBubbleRoutes/updateStudyBubble.js")(router)
  );

  // Task Route
  app.get("/api/tasks", require("./taskRoutes/getAllTasks.js")(router));
  app.post("/api/task", require("./taskRoutes/postTask.js")(router));
  app.delete("/api/task/:id", require("./taskRoutes/deleteTask.js")(router));
  app.put("/api/task/:id", require("./taskRoutes/updateTask.js")(router));

  // User Route
  app.get("/api/users", require("./userRoutes/getAllUsers.js")(router));
  app.post("/api/user/register", require("./userRoutes/userRoutes.js")(router));
};
