const User = require("../../models/user");
const bcrypt = require("bcrypt");

module.exports = function (router) {
  // Register a new user
  router.route("/user/register").post(async (req, res) => {
    if (
      req.body.email == null ||
      req.body.username == null ||
      req.body.password == null
    ) {
      res.status(400);
      res.json({
        message: "400 BAD REQUEST",
        data: {
          error: "All parameters are required",
        },
      });
    } else {
      const currUsersWithEmail = await User.find({ email: req.body.email });

      if (currUsersWithEmail.length > 0) {
        res.status(400);
        res.json({
          message: "400 BAD REQUEST",
          data: {
            error: "User with email already exists",
          },
        });
      } else {
        // Hash user password using bcrypt
        bcrypt.hash(req.body.password, 10, async (err, hash) => {
          const newUser = new User({
            email: req.body.email,
            username: req.body.username,
            password: hash,
          });

          try {
            const dbResponse = await newUser.save();
            res.status(200);
            res.json({ message: "200 OK", data: dbResponse });
          } catch (error) {
            res.status(500);
            res.json({ message: "500 SERVER ERROR", data: error.message });
          }
        });
      }
    }
  });

  // Login user
  router.route("/user/login").post(async (req, res) => {
    if (req.body.email == null || req.body.password == null) {
      res.status(400);
      res.json({
        message: "400 BAD REQUEST",
        data: {
          error: "Email and password are required",
        },
      });
    } else {
      const user = await User.findOne({ email: req.body.email });

      if (user == null) {
        res.status(404);
        res.json({
          message: "404 NOT FOUND",
          data: {
            error: "User with email not found",
          },
        });
      } else {
        // Compare provided plain-text password with stored hashed password
        bcrypt.compare(req.body.password, user.password, (err, cryptres) => {
          if (cryptres) {
            // Password matched
            res.status(200);
            res.json({
              message: "200 OK",
              data: user,
            });
          } else {
            // Password did not match
            res.status(401);
            res.json({
              message: "401 UNAUTHORIZED",
              data: {
                error: "Passwords do not match",
              },
            });
          }
        });
      }
    }
  });

  // Get all users
  router.route("/users").get(async (req, res) => {});

  return router;
};
