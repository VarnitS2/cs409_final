const StudyBubble = require("../../models/studybubble");

module.exports = function (router) {
  var getAllStudyBubblesRoute = router.route("/studybubbles");

  getAllStudyBubblesRoute.get(async function (req, res) {
    let { where, sort, select, skip, limit, count } = req.query;
    where = where ? JSON.parse(where) : {};
    sort = sort ? JSON.parse(sort) : {};
    select = select ? JSON.parse(select) : {};
    skip = skip ? Number(skip) : 0;
    limit = limit ? Number(limit) : null;

    if (where.startDate) {
      let nextDate = new Date(where.startDate);
      nextDate.setDate(nextDate.getDate() + 1);

      where.startDate = { $gte: new Date(where.startDate), $lte: nextDate };
    }

    try {
      const data = count
        ? await StudyBubble.find(where, select)
            .sort(sort)
            .skip(skip)
            .limit(limit)
            .count()
        : await StudyBubble.find(where, select)
            .sort(sort)
            .skip(skip)
            .limit(limit);
      res.status(200).json({
        message: "200 OK",
        data: data,
      });
    } catch (error) {
      res.status(500).json({
        message: "500 SERVER ERROR",
        data: error.message,
      });
    }
  });

  return router;
};
