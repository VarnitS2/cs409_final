const StudyBubble = require("../../models/studybubble");

module.exports = function (router) {
  var updateStudyBubbleRoute = router.route("/studybubble/:id");

  updateStudyBubbleRoute.put(async function (req, res) {
    try {
      const existingStudyBubble = await StudyBubble.findById(req.params.id);
      if (existingStudyBubble == null) {
        res.status(400).json({
          message: "404 NOT FOUND",
          data: "Study Bubble ID doesn't exist",
        });
      } else {
        const updatedStudyBubble = await StudyBubble.updateOne(
          { _id: req.params.id },
          req.body
        );
        res.status(200).json({
          message: "200 OK",
          data: updatedStudyBubble,
        });
      }
    } catch (error) {
      res
        .status(500)
        .json({ message: "500 SERVER ERROR", data: error.message });
    }
  });
  return router;
};
