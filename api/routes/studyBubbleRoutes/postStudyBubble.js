const StudyBubble = require("../../models/studybubble");

module.exports = function (router) {
  var postStudyBubbleRoute = router.route("/studybubble");

  postStudyBubbleRoute.post(async function (req, res) {
    const data = new StudyBubble({
      title: req.body.title,
      startDate: req.body.startDate,
      duration: req.body.duration,
      notes: req.body.notes,
      user_id: req.body.user_id,
      category_id: req.body.category_id,
      time_studied: req.body.time_studied,
    });
    try {
      const dataToSave = await data.save();
      res.status(201).json({
        message: "201 OK",
        data: dataToSave,
      });
    } catch (error) {
      res
        .status(500)
        .json({ message: "500 SERVER ERROR", data: error.message });
    }
  });
  return router;
};
