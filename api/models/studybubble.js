// Load required packages
var mongoose = require("mongoose");

// Define our Study Bubble schema
var StudyBubbleSchema = new mongoose.Schema({
  title: String,
  startDate: Date,
  duration: Number,
  notes: String,
  category_id: String,
  user_id: String,
  time_studied: Number,
});

// Export the Mongoose model
module.exports = mongoose.model("StudyBubble", StudyBubbleSchema);
