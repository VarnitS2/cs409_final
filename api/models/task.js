// Load required packages
var mongoose = require("mongoose");

// Define our Task schema
var TaskSchema = new mongoose.Schema({
  description: String,
  is_complete: Boolean,
  study_bubble_id: String,
});

// Export the Mongoose model
module.exports = mongoose.model("Task", TaskSchema);
