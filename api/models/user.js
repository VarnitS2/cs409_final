// Load required packages
var mongoose = require("mongoose");

// Define our User schema
var UserSchema = new mongoose.Schema({
  email: String,
  username: String,
  password: String,
});

// Export the Mongoose model
module.exports = mongoose.model("User", UserSchema);
