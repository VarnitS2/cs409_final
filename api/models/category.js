// Load required packages
var mongoose = require("mongoose");

// Define our Category schema
var CategorySchema = new mongoose.Schema({
  color: String,
  title: String,
  image: String,
  user_id: String,
});

// Export the Mongoose model
module.exports = mongoose.model("Category", CategorySchema);
