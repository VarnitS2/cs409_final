// Load required packages
var mongoose = require("mongoose");

// Define our Card schema
var CardSchema = new mongoose.Schema({
  front: String,
  back: String,
  study_bubble_id: String,
});

// Export the Mongoose model
module.exports = mongoose.model("Card", CardSchema);
