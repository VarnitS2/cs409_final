import React, { useEffect, useState } from "react";
import "../styles/TasklistView.css";
import { TextInput, Checkbox, ActionIcon, UnstyledButton } from "@mantine/core";
import { List, Stack, Text, Divider } from "@mantine/core";
import { IconPlus, IconSend, IconX } from "@tabler/icons";
import { CloseButton, Group } from "@mantine/core";

type TasklistDetails = {
  studybubbleID: string | undefined;
};

type Task = {
  _id: string;
  description: String;
  is_complete: Boolean;
  study_bubble_id: String;
};

function Tasklist(props: TasklistDetails) {
  const [tasks, setTasks] = useState<Task[]>([]);
  const [newTask, setNewTask] = useState<string>("");
  const [refresh, setRefresh] = useState<boolean>(false);

  const getAllTasks = () => {
    const requestOptions = {
      method: "GET",
      headers: { "Content-Type": "application/json" },
    };

    const path = `http://localhost:3001/api/tasks?where={"study_bubble_id": "${props.studybubbleID}"}`;

    fetch(path, requestOptions)
      .then((response) => response.json())
      .then((data) => {
        if (data.message === "200 OK") {
          setTasks(data.data);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const toggleTaskComplete = (task: Task) => {
    const requestOptions = {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        description: task.description,
        is_complete: !task.is_complete,
        study_bubble_id: task.study_bubble_id,
      }),
    };

    const path = `http://localhost:3001/api/task/${task._id}`;

    fetch(path, requestOptions)
      .then((response) => response.json())
      .then((data) => {
        console.log(data.data);
        if (data.message === "200 OK") {
          setRefresh(!refresh);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const postNewTask = () => {
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        description: newTask,
        is_complete: false,
        study_bubble_id: props.studybubbleID,
      }),
    };

    const path = `http://localhost:3001/api/task`;

    fetch(path, requestOptions)
      .then((response) => response.json())
      .then((data) => {
        if (data.message === "201 OK") {
          setRefresh(!refresh);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const deleteTask = (task: Task) => {
    const requestOptions = {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
    };

    const path = `http://localhost:3001/api/task/${task._id}`;

    fetch(path, requestOptions)
      .then((response) => response.json())
      .then((data) => {
        if (data.message === "200 OK") {
          setRefresh(!refresh);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  useEffect(() => {
    getAllTasks();
  }, [refresh, tasks]);

  return (
    <Stack align="flex-start" justify="flex-start" spacing="lg">
      <div className="tasklist-list">
        {tasks.map((task) => {
          return (
            <Group position="apart" spacing="lg">
              <Checkbox
                size="xl"
                checked={task.is_complete ? true : false}
                value={task.is_complete ? 1 : 0}
                label={task.description}
                color="violet"
                onChange={() => {
                  toggleTaskComplete(task);
                }}
              ></Checkbox>
              <CloseButton
                title="Delete Task"
                size="xl"
                iconSize={20}
                onClick={() => {
                  deleteTask(task);
                }}
              />
            </Group>
          );
        })}
      </div>
      <Group className="new-task" position="apart" spacing={"lg"}>
        <TextInput
          variant="unstyled"
          size="xl"
          icon={<IconPlus color="black" />}
          placeholder="Add Task Here"
          value={newTask}
          onChange={(event) => setNewTask(event.currentTarget.value)}
        />
        <ActionIcon
          color="violet"
          size={60}
          variant="light"
          onClick={() => {
            if (newTask !== "") {
              postNewTask();
              setNewTask("");
            }
          }}
        >
          <IconSend size={24} />
        </ActionIcon>
      </Group>
    </Stack>
  );
}

export default Tasklist;
