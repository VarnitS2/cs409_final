import React from "react";
import { useNavigate } from "react-router-dom";
import { useToggle, upperFirst } from "@mantine/hooks";
import { useForm } from "@mantine/form";
import {
  TextInput,
  PasswordInput,
  Text,
  Paper,
  Group,
  Button,
  Divider,
  Anchor,
  Stack,
} from "@mantine/core";

const AuthForm = (props: {
  onErrorCallback: () => void;
  onIncorrectPasswordCallback: () => void;
  clearFlagsCallback: () => void;
}) => {
  const navigate = useNavigate();
  const [type, toggle] = useToggle(["login", "register"]);

  const form = useForm({
    initialValues: {
      email: "",
      username: "",
      password: "",
    },

    validate: {
      email: (val) => (/^\S+@\S+$/.test(val) ? null : "Invalid email"),
      password: (val) =>
        val.length <= 6
          ? "Password should include at least 6 characters"
          : null,
    },
  });

  const registerUser = () => {
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        username: form.values.username,
        email: form.values.email,
        password: form.values.password,
      }),
    };

    fetch("/api/user/register", requestOptions)
      .then((response) => response.json())
      .then((data) => {
        if (data.message === "200 OK") {
          sessionStorage.setItem("id", data.data._id);
          sessionStorage.setItem("username", data.data.username);
          sessionStorage.setItem("email", data.data.email);
          navigate("/dashboard");
        } else {
          console.log(data.data);
          props.onErrorCallback();
        }
      })
      .catch((error) => {
        console.error(error);
        props.onErrorCallback();
      });
  };

  const loginUser = () => {
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        email: form.values.email,
        password: form.values.password,
      }),
    };

    fetch("/api/user/login", requestOptions)
      .then((response) => response.json())
      .then((data) => {
        if (data.message === "200 OK") {
          sessionStorage.setItem("id", data.data._id);
          sessionStorage.setItem("username", data.data.username);
          sessionStorage.setItem("email", data.data.email);
          navigate("/dashboard");
        } else if (data.message === "401 UNAUTHORIZED") {
          props.onIncorrectPasswordCallback();
        } else {
          console.log(data.data);
          props.onErrorCallback();
        }
      })
      .catch((error) => {
        console.error(error);
        props.onErrorCallback();
      });
  };

  const onSubmitClicked = () => {
    if (type === "register") {
      registerUser();
    } else if (type === "login") {
      loginUser();
    }
  };

  const onTypeChanged = () => {
    toggle();
    props.clearFlagsCallback();
  };

  return (
    <Paper radius="md" p="xl" withBorder>
      <Text size="lg" weight={500}>
        Welcome to Study Bubble
      </Text>

      <Divider my="lg" />

      <form onSubmit={form.onSubmit(onSubmitClicked)}>
        <Stack>
          {type === "register" && (
            <TextInput
              required
              label="Name"
              placeholder="Your name"
              value={form.values.username}
              onChange={(event) =>
                form.setFieldValue("username", event.currentTarget.value)
              }
            />
          )}

          <TextInput
            required
            label="Email"
            placeholder="hello@gmail.com"
            value={form.values.email}
            onChange={(event) =>
              form.setFieldValue("email", event.currentTarget.value)
            }
            error={form.errors.email && "Invalid email"}
          />

          <PasswordInput
            required
            label="Password"
            placeholder="Your password"
            value={form.values.password}
            onChange={(event) =>
              form.setFieldValue("password", event.currentTarget.value)
            }
            error={
              form.errors.password &&
              "Password should include at least 6 characters"
            }
          />
        </Stack>

        <Group position="apart" mt="xl">
          <Anchor
            component="button"
            type="button"
            color="dimmed"
            onClick={onTypeChanged}
            size="xs"
          >
            {type === "register"
              ? "Already have an account? Login"
              : "Don't have an account? Register"}
          </Anchor>

          <Button type="submit">{upperFirst(type)}</Button>
        </Group>
      </form>
    </Paper>
  );
};

export default AuthForm;
