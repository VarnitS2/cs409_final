import React, { useEffect, useState } from "react";
import "../styles/TimerComponent.css";
import "../styles/ModalComponent.css";
import { TextInput, Checkbox, ActionIcon, UnstyledButton } from "@mantine/core";
import { CloseButton, Group } from "@mantine/core";

type ModalDetails = {};

function CustomModal(props: ModalDetails) {
  const [time, setTime] = useState(0);
  const [breakTimeAmount, setBreakTimeAmount] = useState("");
  const [isActive, setIsActive] = useState<boolean>(false);

  useEffect(() => {
    let interval: any = null;
    if (isActive) {
      console.log(Math.floor((time / 60000) % 60) > parseInt(breakTimeAmount));
      interval = setInterval(() => {
        setTime((time) => time + 10);
      }, 10);
    } else {
    }
    return () => {
      clearInterval(interval);
    };
  }, [isActive]);

  return (
    <div className="modal-container">
      {isActive ? (
        <div className="time-container">
          You have been on break for
          <div
            className={
              Math.floor((time / 60000) % 60) > parseInt(breakTimeAmount)
                ? "overtime"
                : "timer"
            }
          >
            <span className="digits">
              {("0" + Math.floor((time / 3600000) % 60)).slice(-2)}:
            </span>
            <span className="digits">
              {("0" + Math.floor((time / 60000) % 60)).slice(-2)}:
            </span>
            <span className="digits">
              {("0" + Math.floor((time / 1000) % 60)).slice(-2)}
            </span>
          </div>
          minutes
        </div>
      ) : (
        <div className="break-time-container">
          Input the number of minutes you would like to take a break for:
          <Group>
            <TextInput
              value={breakTimeAmount}
              onChange={(event) =>
                setBreakTimeAmount(event.currentTarget.value)
              }
              label=""
              placeholder="Number of Minutes"
            />
            Minutes
            <button onClick={() => setIsActive(true)} className="submit-button">
              Submit
            </button>
          </Group>
        </div>
      )}
    </div>
  );
}

export default CustomModal;
