import React, { useEffect, useState } from "react";
import "../styles/TimerComponent.css";
import {
  IconPlayerPlay,
  IconPlayerPause,
  IconEye,
  IconEyeOff,
} from "@tabler/icons";
import { Group, Modal } from "@mantine/core";
import CustomModal from "./ModalComponent";

type TimerDetails = {
  callback: () => void;
  finishedRef: boolean;
  exitSessionCallback: () => void;
  timeStudied: number;
  bubbleID: string;
};

function Timer(props: TimerDetails) {
  const [isActive, setIsActive] = useState<boolean>(false);
  const [isPaused, setIsPaused] = useState<boolean>(true);
  const [open, setOpen] = useState<boolean>(false);
  const [isHidden, setIsHidden] = useState<boolean>(false);
  const [time, setTime] = useState(props.timeStudied);

  useEffect(() => {
    let interval: any = null;
    if (isActive && isPaused === false) {
      interval = setInterval(() => {
        setTime((time) => time + 10);
      }, 10);
    } else {
      clearInterval(interval);
    }
    return () => {
      clearInterval(interval);
    };
  }, [isActive, isPaused]);

  useEffect(() => {
    console.log(time);
    console.log("finished changed");
    if (props.finishedRef) {
      handlePauseResume();
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.finishedRef]);

  const saveTime = () => {
    const requestOptions = {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        time_studied: time / 1000,
      }),
    };

    fetch(`/api/studybubble/${props.bubbleID}`, requestOptions)
      .then((response) => response.json())
      .then((data) => {
        if (data.message === "200 OK") {
          console.log(data);
        } else {
          console.log(data);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const handleStart = () => {
    setIsActive(true);
    setIsPaused(false);
    setIsHidden(false);
  };

  const handlePauseResume = () => {
    setOpen(!open);
    setIsPaused(!isPaused);
  };

  const handleHide = () => {
    setIsHidden(!isHidden);
  };

  const exitSessionConfirmed = () => {
    saveTime();
    props.exitSessionCallback();
  };

  return (
    <div className="timer-container">
      <div className="control-container">
        {isActive ? (
          <div className="button-group">
            <div className="button pause" onClick={handlePauseResume}>
              {isPaused ? (
                <IconPlayerPlay size="sm"></IconPlayerPlay>
              ) : (
                <IconPlayerPause size="sm"></IconPlayerPause>
              )}
            </div>
            <div className="button hide" onClick={handleHide}>
              {isHidden ? <IconEyeOff size="sm" /> : <IconEye size="sm" />}
            </div>
          </div>
        ) : (
          <div className="button start" onClick={handleStart}>
            <IconPlayerPlay size="sm"></IconPlayerPlay>
          </div>
        )}
      </div>
      {isHidden ? (
        <div className="timer" />
      ) : (
        <div className="timer">
          <span className="digits">
            {("0" + Math.floor((time / 3600000) % 60)).slice(-2)}:
          </span>
          <span className="digits">
            {("0" + Math.floor((time / 60000) % 60)).slice(-2)}:
          </span>
          <span className="digits">
            {("0" + Math.floor((time / 1000) % 60)).slice(-2)}.
          </span>
          <span className="millisec">
            {("0" + ((time / 10) % 100)).slice(-2)}
          </span>
        </div>
      )}
      <Modal
        opened={open}
        onClose={() => handlePauseResume()}
        title={props.finishedRef ? "End & Save?" : "Break Time!"}
      >
        {props.finishedRef ? (
          <div className="end-modal-container">
            Are you done studying?
            <Group spacing={"lg"}>
              <button className="no-button" onClick={() => handlePauseResume()}>
                {" "}
                No I'm not done
              </button>
              <button className="yes-button" onClick={exitSessionConfirmed}>
                {" "}
                Yes, I'm done
              </button>
            </Group>
          </div>
        ) : (
          <div className="timer-modal-container">
            <CustomModal></CustomModal>
          </div>
        )}
      </Modal>
    </div>
  );
}

export default Timer;
