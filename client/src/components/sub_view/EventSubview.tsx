import React, { useState, useEffect } from "react";
import "../../styles/EventSubview.css";
import { Textarea, UnstyledButton, Text } from "@mantine/core";
import "../../styles/DashboardView.css";
import "../../styles/TasklistView.css";
import Tasklist from "../TasklistComponent";
import { Tabs } from "@mantine/core";
import { IconCategory, IconCards } from "@tabler/icons";
import {
  IconBrandGoogleAnalytics,
  IconListDetails,
  IconNote,
} from "@tabler/icons";
import TimerComponent from "../TimerComponent";

type StudyBubble = {
  _id: string;
  title: string;
  startDate: Date;
  duration: Number;
  notes: string;
  category_id: string;
  user_id: string;
  time_studied: string;
};

type EventSubviewDetails = {
  studyBubbleID: string;
  exitSessionCallback: () => void;
};

function EventSubview(props: EventSubviewDetails) {
  const [finished, setFinished] = useState<boolean>(false);
  const [currentStudyBubble, setCurrentStudyBubble] = useState<StudyBubble>();
  const [numOfCards, setNumOfCards] = useState<number>(0);

  useEffect(() => {
    getCurrentStudyBubble();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getCurrentStudyBubble = () => {
    const requestOptions = {
      method: "GET",
      headers: { "Content-Type": "application/json" },
    };

    fetch(
      `/api/studybubbles?where={"_id":"${props.studyBubbleID}"}`,
      requestOptions
    )
      .then((response) => response.json())
      .then((data) => {
        if (data.message === "200 OK") {
          const studyBubble: StudyBubble = data.data[0] as StudyBubble;
          setCurrentStudyBubble(studyBubble);
        } else {
          console.log(data.data);
        }
      })
      .catch((error) => {
        console.error(error);
      });

    fetch(
      `/api/cards?count=True&where={"study_bubble_id":"${props.studyBubbleID}"}`,
      requestOptions
    )
      .then((response) => response.json())
      .then((data) => {
        if (data.message === "200 OK") {
          setNumOfCards(data.data);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const updateNotes = (newNotes: string) => {
    console.log(newNotes);

    const requestOptions = {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        _id: currentStudyBubble!._id,
        title: currentStudyBubble!.title,
        startDate: currentStudyBubble!.startDate,
        duration: currentStudyBubble!.duration,
        notes: newNotes,
        category_id: currentStudyBubble!.category_id,
        user_id: currentStudyBubble!.user_id,
        time_studied: currentStudyBubble!.time_studied,
      }),
    };
    fetch(`/api/studybubble/${props.studyBubbleID}`, requestOptions)
      .then((response) => response.json())
      .then((data) => {
        if (data.message === "200 OK") {
          console.log("Updated Notes");
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <div className="event-subview-container">
      <div>
        <div className="event-subview-header">
          <div className="event-subview-title">Work Station...</div>
        </div>
        <Tabs defaultValue="summary" color="violet" variant="pills" radius="xl">
          <Tabs.List grow position="center">
            <Tabs.Tab
              value="summary"
              icon={<IconBrandGoogleAnalytics size={24} />}
            >
              Summary
            </Tabs.Tab>
            <Tabs.Tab value="tasklist" icon={<IconListDetails size={24} />}>
              Tasklist
            </Tabs.Tab>
            <Tabs.Tab value="notes" icon={<IconNote size={24} />}>
              Notes
            </Tabs.Tab>
          </Tabs.List>

          <Tabs.Panel value="summary" pt="xl">
            {currentStudyBubble ? (
              parseInt(currentStudyBubble.time_studied) > 0 ? (
                <div className="summary-container">
                  <div className="summary-statistics-container">
                    <div className="summary-stat-container">
                      <div className="summary-stat-icon-container">
                        <IconCategory className="summary-stat-icon" />
                      </div>

                      <div className="summary-stat-text-container">
                        <div className="summary-stat-text">
                          {Math.floor(
                            parseInt(currentStudyBubble.time_studied) / 60
                          )}
                        </div>

                        <div className="summary-stat-text">Minutes Studied</div>
                      </div>
                    </div>

                    <div className="summary-stat-container">
                      <div className="summary-stat-icon-container">
                        <IconCards className="summary-stat-icon" />
                      </div>

                      <div className="summary-stat-text-container">
                        <div className="summary-stat-text">{numOfCards}</div>

                        <div className="summary-stat-text">flash cards</div>
                      </div>
                    </div>
                  </div>
                </div>
              ) : (
                <TimerComponent
                  callback={() => {
                    setFinished(false);
                  }}
                  finishedRef={finished}
                  exitSessionCallback={props.exitSessionCallback}
                  timeStudied={parseInt(currentStudyBubble.time_studied)}
                  bubbleID={currentStudyBubble._id}
                />
              )
            ) : (
              <div></div>
            )}
          </Tabs.Panel>

          <Tabs.Panel value="tasklist" pt="xl">
            <Tasklist studybubbleID={currentStudyBubble?._id}></Tasklist>
          </Tabs.Panel>

          <Tabs.Panel value="notes" pt="xl">
            <div className="notes-container">
              <div className="notes">
                <Textarea
                  placeholder="Add Notes Here..."
                  label=""
                  minRows={8}
                  defaultValue={currentStudyBubble?.notes as string}
                  onBlur={(event) => {
                    updateNotes(event?.currentTarget.value);
                  }}
                ></Textarea>
              </div>
              <UnstyledButton
                className="clear-button"
                onClick={() => {
                  updateNotes("");
                }}
              >
                <Text size="md">Clear Notes</Text>
              </UnstyledButton>
            </div>
          </Tabs.Panel>
        </Tabs>
      </div>
      <UnstyledButton
        className="clear-button"
        onClick={() => {
          if (parseInt(currentStudyBubble!.time_studied) > 0) {
            props.exitSessionCallback();
          } else {
            setFinished(true);
          }
        }}
      >
        <Text size="md">Exit Session</Text>
      </UnstyledButton>
    </div>
  );
}

export default EventSubview;
