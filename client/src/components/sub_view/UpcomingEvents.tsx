import React, { Key, useEffect, useState } from "react";
import { Divider } from "@mantine/core";
import "../../styles/UpcomingEvents.css";

type StudyBubble = {
  _id: string;
  title: String;
  startDate: Date;
  duration: number;
  notes: String;
  category_id: String;
  user_id: String;
  time_studied: String;
};

type UpcomingEventDetails = {
  startDate: string;
  bubbleClickedCallback: (bubbleID: string, bubbleStartDate: Date) => void;
};

const UpcomingEvents = (props: UpcomingEventDetails) => {
  const [studybubbles, setStudyBubbles] = useState<StudyBubble[]>([]);

  const [todayFlag, setTodayFlag] = useState(false);
  const [emptyFlag, setEmptyFlag] = useState(false);
  const [errorFlag, setErrorFlag] = useState(false);

  useEffect(() => {
    getDayStudyBubbles();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.startDate]);

  const clearFlags = () => {
    setTodayFlag(false);
    setEmptyFlag(false);
    setErrorFlag(false);
  };

  const getDayStudyBubbles = () => {
    const requestOptions = {
      method: "GET",
      headers: { "Content-Type": "application/json" },
    };

    fetch(
      `/api/studybubbles?where={"startDate":"${new Date(
        props.startDate
      ).toISOString()}", "user_id":"${sessionStorage.getItem("id")}"}&sort`,
      requestOptions
    )
      .then((response) => response.json())
      .then((data) => {
        clearFlags();

        if (data.message === "200 OK") {
          if (data.data.length === 0) {
            getUpcomingStudyBubbles();
          } else {
            setStudyBubbles(data.data);
            setTodayFlag(true);
          }
        } else {
          console.log(data);
          setErrorFlag(true);
        }
      })
      .catch((error) => {
        console.error(error);
        setErrorFlag(true);
      });
  };

  const getUpcomingStudyBubbles = () => {
    const requestOptions = {
      method: "GET",
      headers: { "Content-Type": "application/json" },
    };

    fetch(
      `/api/studybubbles?where={"user_id":"${sessionStorage.getItem(
        "id"
      )}"}&sort&limit=3`,
      requestOptions
    )
      .then((response) => response.json())
      .then((data) => {
        clearFlags();

        if (data.message === "200 OK") {
          if (data.data.length === 0) {
            setEmptyFlag(true);
          } else {
            setStudyBubbles(data.data);
          }
        } else {
          setErrorFlag(true);
        }
      })
      .catch((error) => {
        console.error(error);
        setErrorFlag(true);
      });
  };

  return (
    <div className="upcoming-event-container">
      {todayFlag ? (
        <div className="upcoming-event-title">Today</div>
      ) : (
        <div className="upcoming-none-today-container">
          <div className="upcoming-event-title">Today</div>
          <div className="upcoming-none-text">No bubbles on this day</div>
          <div className="upcoming-event-title">Other</div>
        </div>
      )}

      <div className="upcoming-event-content">
        {errorFlag ? (
          <div className="upcoming-event-empty">Error fetching bubbles</div>
        ) : emptyFlag ? (
          <div className="upcoming-event-empty">No other bubbles</div>
        ) : (
          <div className="upcoming-event-container">
            {studybubbles.map((studybubble) => (
              <div
                key={studybubble._id as Key}
                className="event-card-container"
                onClick={() =>
                  props.bubbleClickedCallback(
                    studybubble._id,
                    studybubble.startDate
                  )
                }
              >
                <div className="event-date-container">
                  <div className="event-date-text">
                    {new Date(studybubble.startDate).getDate()}
                  </div>

                  <div className="event-date-text">
                    {
                      new Date(studybubble.startDate)
                        .toDateString()
                        .split(" ")[0]
                    }
                  </div>
                </div>

                <div className="event-text-container">
                  <div className="event-text-title">{studybubble.title}</div>

                  <div className="event-time-duration">
                    {new Date(studybubble.startDate)
                      .toTimeString()
                      .split(" ")[0]
                      .split(":")
                      .splice(0, 2)
                      .join(":")}
                    <Divider
                      orientation="vertical"
                      size="sm"
                      className="event-divider"
                    />

                    {(studybubble.duration / 60 / 60).toFixed(2) + " hrs"}
                  </div>
                </div>
              </div>
            ))}
          </div>
        )}
      </div>
    </div>
  );
};

export default UpcomingEvents;
