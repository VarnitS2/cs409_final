import React, { useEffect, useState } from "react";
import "../../styles/TasklistView.css";
import { UnstyledButton } from "@mantine/core";
import { Text } from "@mantine/core";
import { IconX } from "@tabler/icons";
import Tasklist from "../TasklistComponent";

type TasklistViewDetails = {
  studybubbleID: string;
  isAvailable: boolean;
  closeCallback: () => void;
  enterSessionCallback: (bubbleID: string) => void;
};

type Task = {
  _id: string;
  description: String;
  is_complete: Boolean;
  study_bubble_id: String;
};

function TasklistView(props: TasklistViewDetails) {
  return (
    <div className="tasklist-view">
      <div className="tasklist-header">
        <div className="tasklist-title">Focus Time...</div>

        <IconX className="tasklist-close-icon" onClick={props.closeCallback} />
      </div>
      <div className="tasklist-container">
        <Tasklist studybubbleID={props.studybubbleID}></Tasklist>

        {props.isAvailable ? (
          <UnstyledButton
            className="enter-button"
            onClick={() => props.enterSessionCallback(props.studybubbleID)}
          >
            <Text size="lg">Enter Session</Text>
          </UnstyledButton>
        ) : null}
      </div>
    </div>
  );
}

export default TasklistView;
