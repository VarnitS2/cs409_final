import React from "react";
import "../../styles/AddEvent.css";
import { DatePicker } from "@mantine/dates";
import { TextInput, Button, Group } from "@mantine/core";
import { useForm } from "@mantine/form";
import { TimeInput } from "@mantine/dates";
import { ColorSwatch } from "@mantine/core";
import { ColorInput } from "@mantine/core";
import { IconX } from "@tabler/icons";

type AddEventDetails = {
  date: string;
  callbackRefresh: () => void;
  categoryColors: string[];
};

function AddEvent(props: AddEventDetails) {
  const form = useForm({
    initialValues: {
      title: "",
      date: new Date(props.date),
      start: new Date(props.date),
      duration: "",
      category: "",
    },
  });

  const swatches = props.categoryColors.map((color) => (
    <ColorSwatch
      key={color}
      color={color}
      size={20}
      onClick={() => {
        form.setFieldValue("category", color);
      }}
    />
  ));

  const startSubmitSequence = () => {
    const requestOptions = {
      method: "GET",
      headers: { "Content-Type": "application/json" },
    };

    const path = `/api/categories?limit=1&where={"color": "%23${form.values.category.replace(
      "#",
      ""
    )}"}`;

    fetch(path, requestOptions)
      .then((response) => response.json())
      .then((data) => {
        // console.log(data.data);
        if (data.message === "200 OK" && data.data.length > 0) {
          submitEvent(data.data[0]._id);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const submitEvent = (categoryID: string) => {
    if (form.isValid() && categoryID !== "") {
      const requestOptions = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          title: form.values.title,
          startDate:
            form.values.date.toISOString().split("T")[0] +
            "T" +
            form.values.start.toISOString().split("T")[1],
          duration: `${parseInt(form.values.duration) * 60 * 60}`,
          notes: "",
          category_id: `${categoryID}`,
          user_id: sessionStorage.getItem("id"),
          time_studied: 0,
        }),
      };

      console.log(requestOptions.body.toString());

      fetch(`/api/studybubble`, requestOptions)
        .then((response) => response.json())
        .then((data) => {
          if (data.message === "201 OK") {
            console.log("SUCCESS MADE BUBBLE");
            props.callbackRefresh();
          }
        })
        .catch((error) => {
          console.error(error);
        });

      return [];
    }
  };

  return (
    <div className="add-event-container">
      <div className="add-event-header">
        <div className="add-event-title">Adding...</div>

        <IconX
          className="add-event-close-icon"
          onClick={props.callbackRefresh}
        />
      </div>

      <div className="add-event-form">
        <form onSubmit={form.onSubmit((values) => startSubmitSequence())}>
          <TextInput
            label="Title"
            required
            placeholder="Study Bubble Goal ..."
            value={form.values.title}
            onChange={(event) =>
              form.setFieldValue("title", event.currentTarget.value)
            }
            size="lg"
          />

          <DatePicker
            label="Date"
            size="lg"
            value={form.values.date}
            onChange={(event) => {
              if (event) {
                form.setFieldValue("date", event);
              }
            }}
          />

          <TimeInput
            label="Start Time"
            placeholder="Pick time"
            value={form.values.start}
            onChange={(event) => {
              if (event) {
                form.setFieldValue("start", event);
              }
            }}
            size="lg"
          />

          <TextInput
            label="Duration"
            size="lg"
            value={form.values.duration}
            onChange={(event) => {
              form.setFieldValue("duration", event.currentTarget.value);
            }}
          />

          <ColorInput
            label="Category"
            placeholder="Pick category"
            withEyeDropper={false}
            withPicker={false}
            value={form.values.category}
            swatches={props.categoryColors}
            size="lg"
            onChange={(event) => {
              form.setFieldValue("category", event);
            }}
            rightSection={<div className="color-section">{swatches}</div>}
          />

          <Group position="right" mt="md">
            <Button type="submit">Submit</Button>
          </Group>
        </form>
      </div>
    </div>
  );
}

export default AddEvent;
