import React, { useState, useEffect } from "react";
import { Divider } from "@mantine/core";
import "../../styles/CategoryView.css";

type StudyBubble = {
  _id: string;
  title: String;
  startDate: Date;
  duration: Number;
  notes: String;
  category_id: String;
  user_id: String;
  time_studied: String;
};

type CategoryViewDetails = {
  categoryID: number;
  categoryTitle: string;
  categoryColor: string;
  bubbleClickedCallback: (bubbleID: string, bubbleStartDate: Date) => void;
};

const CategoryView = (props: CategoryViewDetails) => {
  const [categoryBubbles, setCategoryBubbles] = useState<StudyBubble[]>([]);

  const [emptyFlag, setEmptyFlag] = useState<boolean>(true);
  const [errorFlag, setErrorFlag] = useState<boolean>(false);

  useEffect(() => {
    getCategoryBubbles();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const clearFlags = () => {
    setEmptyFlag(false);
    setErrorFlag(false);
  };

  const getCategoryBubbles = () => {
    const requestOptions = {
      method: "GET",
      headers: { "Content-Type": "application/json" },
    };

    fetch(
      `/api/studybubbles?where={"category_id":"${
        props.categoryID
      }", "user_id":"${sessionStorage.getItem("id")}"}&sort`,
      requestOptions
    )
      .then((response) => response.json())
      .then((data) => {
        clearFlags();

        if (data.message === "200 OK") {
          if (data.data.length === 0) {
            setEmptyFlag(true);
          } else {
            setCategoryBubbles(data.data);
          }
        } else {
          console.log(data.data);
          setErrorFlag(true);
        }
      })
      .catch((error) => {
        console.error(error);
        setErrorFlag(true);
      });
  };

  return (
    <div className="category-view-container">
      <div className="category-title">{props.categoryTitle}</div>

      <div className="category-divider">
        <Divider my="xl" size="md" />
      </div>

      <div className="category-content-container">
        {errorFlag ? (
          <div className="category-empty-content">Error retrieving bubbles</div>
        ) : emptyFlag ? (
          <div className="category-empty-content">No bubbles yet</div>
        ) : (
          categoryBubbles.map((bubble) => (
            <div
              className="category-bubble-card"
              style={{ backgroundColor: `${props.categoryColor}55` }}
              onClick={() => props.bubbleClickedCallback(bubble._id, bubble.startDate)}
            >
              <div className="category-bubble-info-column">
                <div className="category-bubble-title">{bubble.title}</div>
              </div>

              <div className="category-bubble-date-column">
                <div className="category-bubble-date-container">
                  <div className="category-bubble-date-text-top">
                    {new Date(bubble.startDate).toDateString().split(" ")[1]}{" "}
                    {new Date(bubble.startDate).getDate()}
                  </div>

                  <div className="category-bubble-date-text-bottom">
                    {new Date(bubble.startDate).toDateString().split(" ")[0]}
                  </div>
                </div>
              </div>
            </div>
          ))
        )}
      </div>
    </div>
  );
};

export default CategoryView;
