import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { Button } from "@mantine/core";
import "../../styles/UserViewStyles.css";

const UserView = (props: { logoutCallback: () => void }) => {
  const navigate = useNavigate();

  const logoutUser = () => {
    props.logoutCallback();
    
    sessionStorage.removeItem("id");
    sessionStorage.removeItem("username");
    sessionStorage.removeItem("email");

    navigate("/");
  };

  return (
    <div className="user-view-container">
      <div className="user-title">User Options</div>

      <div className="user-content-container">
        <div className="user-content-text">
          Currently signed in as
          <div className="user-content-text-name">
            {sessionStorage.getItem("username")}
          </div>
        </div>

        <div className="user-logout-text">Sign out of your current session</div>

        <div className="user-logout-button">
          <Button color="red" size="lg" onClick={logoutUser}>
            Logout
          </Button>
        </div>
      </div>
    </div>
  );
};

export default UserView;
