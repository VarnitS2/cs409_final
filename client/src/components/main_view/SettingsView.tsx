import React, { useState, useEffect } from "react";
import {
  SimpleGrid,
  Image,
  Button,
  ColorSwatch,
  TextInput,
  ColorInput,
  Group,
} from "@mantine/core";
import "../../styles/SettingsViewStyles.css";
import { useForm } from "@mantine/form";

type Category = {
  _id: number;
  color: string;
  title: string;
  image: string;
  user_id: Number;
};
type SettingsDetail = {
  callback: () => void;
};
const SettingsView = (props: SettingsDetail) => {
  const categoryForm = useForm({
    initialValues: {
      title: "",
      color: "",
      image: "",
    },
  });

  const postNewCategory = () => {
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        title: categoryForm.values.title,
        color: categoryForm.values.color,
        image: categoryForm.values.image,
        user_id: sessionStorage.getItem("id"),
      }),
    };

    const path = `http://localhost:3001/api/category`;

    fetch(path, requestOptions)
      .then((response) => response.json())
      .then((data) => {
        if (data.message === "201 OK") {
          props.callback();
          console.log("CREATED CATEGORY");
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const colors = [
    "#E84395",
    "#6F57CD",
    "#56D641",
    "#F5C341",
    "#41A0D6",
    "#F22929",
  ];

  const swatches = colors.map((color) => (
    <ColorSwatch
      key={color}
      color={color}
      size={20}
      onClick={() => {
        categoryForm.setFieldValue("category", color);
      }}
    />
  ));
  return (
    <div className="settings-view-container">
      <div className="settings-title">Category Settings</div>

      <div className="settings-content-container">
        <form onSubmit={categoryForm.onSubmit((values) => postNewCategory())}>
          <TextInput
            label="Title"
            required
            placeholder="Study Bubble Goal ..."
            value={categoryForm.values.title}
            onChange={(event) =>
              categoryForm.setFieldValue("title", event.currentTarget.value)
            }
            size="lg"
          />

          <TextInput
            label="Image (public url link)"
            size="lg"
            value={categoryForm.values.image}
            onChange={(event) => {
              categoryForm.setFieldValue("image", event.currentTarget.value);
            }}
          />

          <ColorInput
            label="Category Color"
            placeholder="Pick color"
            withEyeDropper={false}
            withPicker={false}
            value={categoryForm.values.color}
            swatches={colors}
            size="lg"
            onChange={(event) => {
              categoryForm.setFieldValue("color", event);
            }}
          />

          <Group position="right" mt="md">
            <Button type="submit">Submit</Button>
          </Group>
        </form>
      </div>
    </div>
  );
};

export default SettingsView;
