import React, { Key, useEffect, useState } from "react";
import "../../styles/DashboardView.css";
import { Carousel } from "@mantine/carousel";
import { Image, Button } from "@mantine/core";
import { IconCategory, IconChartBubble, IconClock } from "@tabler/icons";

type Category = {
  _id: number;
  color: string;
  title: string;
  image: string;
  user_id: Number;
};

type DashboardDetails = {
  categories: Category[];
  categoryClicked: (
    categoryID: number,
    categoryTitle: string,
    categoryColor: string
  ) => void;
};

const DashboardView = (props: DashboardDetails) => {
  const [categoryCount, setCategoryCount] = useState<number>(0);
  const [studyBubbleCount, setStudyBubbleCount] = useState<number>(0);
  const [minuteCount, setMinuteCount] = useState<number>(0);

  useEffect(() => {
    getDashboardStats();
  }, []);

  const getDashboardStats = () => {
    const requestOptions = {
      method: "GET",
      headers: { "Content-Type": "application/json" },
    };

    fetch(
      `/api/categories?where={"user_id":"${sessionStorage.getItem(
        "id"
      )}"}&count=True`,
      requestOptions
    )
      .then((response) => response.json())
      .then((data) => {
        if (data.message === "200 OK") {
          setCategoryCount(data.data);
        }
      })
      .catch((error) => {
        console.error(error);
      });

    fetch(
      `/api/studybubbles?where={"user_id":"${sessionStorage.getItem(
        "id"
      )}"}&count=True`,
      requestOptions
    )
      .then((response) => response.json())
      .then((data) => {
        if (data.message === "200 OK") {
          setStudyBubbleCount(data.data);
        }
      })
      .catch((error) => {
        console.error(error);
      });

    fetch(
      `/api/studybubbles?where={"user_id":"${sessionStorage.getItem(
        "id"
      )}"}&select={"time_studied": 1, "_id":0}`,
      requestOptions
    )
      .then((response) => response.json())
      .then((data) => {
        if (data.message === "200 OK") {
          const responseData: { time_studied: number }[] = data.data;
          let sum: number = 0;
          responseData.forEach((dataPiece: { time_studied: number }) => {
            sum += dataPiece["time_studied"];
          });
          sum = Math.floor(sum / 60);
          setMinuteCount(sum);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <div className="dashboard-view-container">
      <div className="dashboard-header">
        <div className="intro-container">
          <div className="intro-text">
            Hi {sessionStorage.getItem("username")},
          </div>
        </div>

        <div className="statistics-container">
          <div className="stat-container">
            <div className="stat-icon-container">
              <IconCategory className="stat-icon" />
            </div>

            <div className="stat-text-container">
              <div className="stat-text">{categoryCount}</div>

              <div className="stat-text">categories</div>
            </div>
          </div>

          <div className="stat-container">
            <div className="stat-icon-container">
              <IconChartBubble className="stat-icon" />
            </div>

            <div className="stat-text-container">
              <div className="stat-text">{studyBubbleCount}</div>

              <div className="stat-text">study bubbles</div>
            </div>
          </div>

          <div className="stat-container">
            <div className="stat-icon-container">
              <IconClock className="stat-icon" />
            </div>

            <div className="stat-text-container">
              <div className="stat-text">{minuteCount}</div>

              <div className="stat-text">minutes</div>
            </div>
          </div>
        </div>
      </div>

      <div className="carousel-container">
        {props.categories.length !== 0 ? (
          <div className="carousel">
            <Carousel slideSize="33%" align="center" loop controlSize={50}>
              {props.categories.map((category) => (
                <Carousel.Slide
                  sx={{ backgroundColor: `${category.color}55` }}
                  key={category.title as Key}
                  className="carousel-slide"
                >
                  <div className="carousel-image-container">
                    <Image
                      width={150}
                      height={150}
                      radius={20}
                      src={category.image}
                      alt="Category"
                      withPlaceholder
                    />
                  </div>

                  <div className="carousel-title">{category.title}</div>

                  <Button
                    className="view-all-button"
                    variant="white"
                    radius="xl"
                    onClick={() => {
                      props.categoryClicked(
                        category._id,
                        category.title,
                        category.color
                      );
                    }}
                  >
                    View All
                  </Button>
                </Carousel.Slide>
              ))}
            </Carousel>
          </div>
        ) : (
          <div className="no-category"> No Categories Found </div>
        )}
      </div>
    </div>
  );
};

export default DashboardView;
