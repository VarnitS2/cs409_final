import React, { useEffect, useState } from "react";
import "../../styles/StudyBubbleView.css";
import { Textarea, CloseButton } from "@mantine/core";

type StudyBubble = {
  _id: string;
  title: string;
  startDate: Date;
  duration: Number;
  notes: string;
  category_id: string;
  user_id: string;
  time_studied: string;
};

type Card = {
  _id: string;
  front: string;
  back: string;
  study_bubble_id: string;
};

type StudyBubbleViewDetails = {
  studyBubbleID: string;
  categoryColor: string;
};

const StudyBubbleView = (props: StudyBubbleViewDetails) => {
  const [cards, setCards] = useState<Card[]>([]);
  const [currentStudyBubble, setCurrentStudyBubble] = useState<StudyBubble>();

  const [errorFlag, setErrorFlag] = useState<boolean>(false);

  useEffect(() => {
    getCurrentStudyBubble();
  }, []);

  const getCurrentStudyBubble = () => {
    const requestOptions = {
      method: "GET",
      headers: { "Content-Type": "application/json" },
    };

    fetch(
      `/api/studybubbles?where={"_id":"${props.studyBubbleID}"}`,
      requestOptions
    )
      .then((response) => response.json())
      .then((data) => {
        if (data.message === "200 OK") {
          setCurrentStudyBubble(data.data[0]);
          getAllCards();
        } else {
          console.log(data.data);
          setErrorFlag(true);
        }
      })
      .catch((error) => {
        console.error(error);
        setErrorFlag(true);
      });
  };

  const getAllCards = () => {
    const requestOptions = {
      method: "GET",
      headers: { "Content-Type": "application/json" },
    };

    fetch(
      `/api/cards?where={"study_bubble_id":"${props.studyBubbleID}"}`,
      requestOptions
    )
      .then((response) => response.json())
      .then((data) => {
        if (data.message === "200 OK") {
          setCards(data.data as Card[]);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const addNewCard = () => {
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        front: "",
        back: "",
        study_bubble_id: `${props.studyBubbleID}`,
      }),
    };

    fetch(`/api/card`, requestOptions)
      .then((response) => response.json())
      .then((data) => {
        if (data.message === "201 OK") {
          getAllCards();
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const updateCard = (newCard: Card) => {
    const requestOptions = {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        front: newCard.front,
        back: newCard.back,
        study_bubble_id: `${props.studyBubbleID}`,
      }),
    };

    fetch(`/api/card/${newCard._id}`, requestOptions)
      .then((response) => response.json())
      .then((data) => {
        if (data.message === "200 OK") {
          getAllCards();
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const createCard = (card: Card): React.ReactNode => {
    var front = card.front;
    var back = card.back;
    return (
      <div className="card-row">
        <div className="card-container">
          <Textarea
            placeholder="Add Information..."
            label="Front Card"
            minRows={5}
            defaultValue={front}
            onChange={(event) => {
              front = event.currentTarget.value;
            }}
            onBlur={() => {
              const newCard: Card = {
                _id: card._id,
                front: front,
                back: back,
                study_bubble_id: card.study_bubble_id,
              };
              updateCard(newCard);
            }}
          ></Textarea>
        </div>
        <div className="card-container">
          <Textarea
            placeholder="Add Information..."
            label="Back Card"
            minRows={4}
            defaultValue={back}
            onChange={(event) => {
              back = event.currentTarget.value;
            }}
            onBlur={() => {
              const newCard: Card = {
                _id: card._id,
                front: front,
                back: back,
                study_bubble_id: card.study_bubble_id,
              };
              updateCard(newCard);
            }}
          ></Textarea>
        </div>
        <CloseButton
          title="Delete Task"
          size="xl"
          iconSize={20}
          onClick={() => {
            // TODO: Create delete
            // deleteCard(card);
          }}
        />
      </div>
    );
  };

  return (
    <div className="study-bubble-view-container">
      <div className="study-bubble-header">{currentStudyBubble?.title}</div>
      <div className="both-card-container">
        {cards.map((card) => {
          return createCard(card);
        })}
      </div>
      <div className="add-card-container">
        <button
          style={{ backgroundColor: `${props.categoryColor}33` }}
          className="add-card-button"
          onClick={() => {
            addNewCard();
          }}
        >
          Add Card
        </button>
      </div>
    </div>
  );
};

export default StudyBubbleView;
