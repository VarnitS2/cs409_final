import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import "../styles/Layout.css";
import AccountCircleOutlinedIcon from "@mui/icons-material/AccountCircleOutlined";
import SettingsOutlinedIcon from "@mui/icons-material/SettingsOutlined";
import ControlPointIcon from "@mui/icons-material/ControlPoint";
import { Calendar } from "@mantine/dates";
import { Popover } from "@mantine/core";
import SettingsView from "../components/main_view/SettingsView";

type LayoutDetails = {
  mainView: React.ReactNode;
  subView: React.ReactNode;
  addEventCallbackRef: () => void;
  dateChangedCallback: (newDate: Date | null) => void;
  userViewClickedCallback: () => void;
  settingsViewClickedCallback: () => void;
  date: number;
  subViewSessionFlag: boolean;
};

function Layout(props: LayoutDetails) {
  const [value, setValue] = useState<Date | null>(new Date(props.date));
  const [openSettings, setOpenSettings] = useState(false);

  const navigate = useNavigate();

  useEffect(() => {
    if (sessionStorage.getItem("email") == null) {
      navigate("/");
    }
  });

  const onCalendarDateClicked = (val: Date | null) => {
    setValue(val);
    props.dateChangedCallback(val);
  };

  return (
    <div className="layout-main-container">
      <div className="layout-navbar">
        <button
          className="navbar-button"
          onClick={props.userViewClickedCallback}
          disabled={props.subViewSessionFlag}
        >
          <AccountCircleOutlinedIcon sx={{ width: "80%", height: "80%" }} />
        </button>

        <button
          className="navbar-button"
          onClick={() => props.addEventCallbackRef()}
          disabled={props.subViewSessionFlag}
        >
          <ControlPointIcon sx={{ width: "80%", height: "80%" }} />
        </button>

        <Popover
          opened={openSettings}
          width={400}
          trapFocus
          position="right"
          withArrow
          shadow="md"
        >
          <Popover.Target>
            <button
              className="navbar-button"
              onClick={() => setOpenSettings((o) => !o)}
              disabled={props.subViewSessionFlag}
            >
              <SettingsOutlinedIcon sx={{ width: "80%", height: "80%" }} />
            </button>
          </Popover.Target>
          <Popover.Dropdown>
            <SettingsView
              callback={() => {
                setOpenSettings((o) => !o);
              }}
            ></SettingsView>
          </Popover.Dropdown>
        </Popover>
      </div>

      <div className="layout-main-view">{props.mainView}</div>

      <div className="layout-sub-container">
        <div className="layout-calendar">
          {props.subViewSessionFlag ? (
            <div className="layout-calendar-disabled"></div>
          ) : null}
          <Calendar
            size="lg"
            value={value}
            firstDayOfWeek="sunday"
            onChange={onCalendarDateClicked}
          />
        </div>
        <div className="layout-sub-view">{props.subView} </div>
      </div>
    </div>
  );
}

export default Layout;
