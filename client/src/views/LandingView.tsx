import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { Alert } from "@mantine/core";
import { IconAlertCircle } from "@tabler/icons";
import AuthForm from "../components/AuthForm";
import "../styles/LandingStyles.scss";

const LandingView = () => {
  const [errorFlag, setErrorFlag] = useState(false);
  const [incorrectPasswordFlag, setIncorrectPasswordFlag] = useState(false);

  const navigate = useNavigate();

  useEffect(() => {
    if (sessionStorage.getItem("email")) {
      navigate("/dashboard");
    }
  });

  const onErrorCallback = () => {
    clearFlags();
    setErrorFlag(true);
  };

  const onIncorrectPasswordCallback = () => {
    clearFlags();
    setIncorrectPasswordFlag(true);
  };

  const clearFlags = () => {
    setErrorFlag(false);
    setIncorrectPasswordFlag(false);
  };

  return (
    <div className="landing">
      <div className="landing__title-container">
        <div className="landing__title">Study Bubble</div>
      </div>

      <div className="landing__login-container">
        {errorFlag ? (
          <div className="landing__login-error-container">
            <Alert
              icon={<IconAlertCircle size={16} />}
              title="Error"
              color="red"
              radius="md"
            >
              Something went wrong!
            </Alert>
          </div>
        ) : incorrectPasswordFlag ? (
          <div className="landing__login-error-container">
            <Alert
              icon={<IconAlertCircle size={16} />}
              title="Error"
              color="red"
              radius="md"
            >
              Incorrect password
            </Alert>
          </div>
        ) : null}

        <AuthForm
          onErrorCallback={onErrorCallback}
          onIncorrectPasswordCallback={onIncorrectPasswordCallback}
          clearFlagsCallback={clearFlags}
        />
      </div>
    </div>
  );
};

export default LandingView;
