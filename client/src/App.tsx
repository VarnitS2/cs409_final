import React, { useEffect, useState } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import "./App.css";
import Layout from "../src/views/Layout";
import DashboardView from "./components/main_view/DashboardView";
import LandingView from "./views/LandingView";
import AddEvent from "./components/sub_view/AddEvent";
import UpcomingEvents from "./components/sub_view/UpcomingEvents";
import CategoryView from "./components/main_view/CategoryView";
import TasklistView from "./components/sub_view/TasklistView";
import StudyBubbleView from "./components/main_view/StudyBubbleView";
import EventSubview from "./components/sub_view/EventSubview";
import UserView from "./components/main_view/UserView";

type Category = {
  _id: number;
  color: string;
  title: string;
  image: string;
  user_id: Number;
};

function App() {
  const [categories, setCategories] = useState<Category[]>([]);
  const [categoryColors, setCategoryColors] = useState<string[]>([]);

  const [selectedDate, setSelectedDate] = useState<Date | null>(new Date());

  const [selectedCategoryID, setSelectedCategoryID] = useState<number>();
  const [selectedCategoryTitle, setSelectedCategoryTitle] = useState<string>();
  const [selectedCategoryColor, setSelectedCategoryColor] = useState<string>();
  const [selectedBubbleID, setSelectedBubbleID] = useState<string>();
  const [selectedBubbleStartDate, setSelectedBubbleStartDate] =
    useState<Date>();

  const [mainViewCategoryFlag, setMainViewCategoryFlag] =
    useState<boolean>(false);

  const [mainViewSessionFlag, setMainViewSessionFlag] =
    useState<boolean>(false);

  const [mainViewUserFlag, setMainViewUserFlag] = useState<boolean>(false);
  const [mainViewSettingsFlag, setMainViewSettingsFlag] =
    useState<boolean>(false);

  const [subViewBubbleFlag, setSubViewBubbleFlag] = useState<boolean>(false);
  const [subViewSessionFlag, setSubViewSessionFlag] = useState<boolean>(false);

  const [addEvent, setAddEvent] = useState<Boolean>(false);

  useEffect(() => {
    getAllCategories();
    selectedDate?.setHours(0);
    selectedDate?.setMinutes(0);
    selectedDate?.setSeconds(0);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getAllCategories = (): Category[] => {
    const requestOptions = {
      method: "GET",
      headers: { "Content-Type": "application/json" },
    };

    fetch(
      `/api/categories?where={"user_id":"${sessionStorage.getItem("id")}"}`,
      requestOptions
    )
      .then((response) => response.json())
      .then((data) => {
        if (data.message === "200 OK") {
          setCategories(data.data);
          let tempColors: string[] = [];
          data.data.forEach((category: Category) => {
            tempColors.push(category.color);
          });
          setCategoryColors(tempColors);
        }
      })
      .catch((error) => {
        console.error(error);
      });
    return [];
  };

  const mainViewCloseCallback = () => {
    setMainViewCategoryFlag(false);
    setMainViewSessionFlag(false);
    setMainViewUserFlag(false);
    setMainViewSettingsFlag(false);
  };

  const subViewCloseCallback = () => {
    setAddEvent(false);
    setSubViewBubbleFlag(false);
    setSubViewSessionFlag(false);
  };

  const addEventCallback = () => {
    subViewCloseCallback();
    setAddEvent(!addEvent);
  };

  const dateChangedCallback = (newDate: Date | null) => {
    mainViewCloseCallback();
    subViewCloseCallback();
    setSelectedDate(newDate);
  };

  const categoryClickedCallback = (
    categoryID: number,
    categoryTitle: string,
    categoryColor: string
  ) => {
    setSelectedCategoryID(categoryID);
    setSelectedCategoryTitle(categoryTitle);
    setSelectedCategoryColor(categoryColor);
    mainViewCloseCallback();
    setMainViewCategoryFlag(true);
  };

  const bubbleClickedCallback = (bubbleID: string, bubbleStartDate: Date) => {
    setSelectedBubbleID(bubbleID);
    setSelectedBubbleStartDate(bubbleStartDate);
    subViewCloseCallback();
    setSubViewBubbleFlag(true);
  };

  const enterSessionClickedCallback = (bubbleID: string) => {
    setSelectedBubbleID(bubbleID);
    mainViewCloseCallback();
    subViewCloseCallback();
    setMainViewSessionFlag(true);
    setSubViewSessionFlag(true);
  };

  const exitSessionCallback = () => {
    mainViewCloseCallback();
    subViewCloseCallback();
  };

  const userViewClicked = () => {
    mainViewCloseCallback();
    setMainViewUserFlag(!mainViewUserFlag);
  };

  const logoutCallback = () => {
    mainViewCloseCallback();
    subViewCloseCallback();
  };

  const settingsViewClicked = () => {
    mainViewCloseCallback();
    setMainViewSettingsFlag(!mainViewSettingsFlag);
  };

  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/" element={<LandingView />} />

          <Route
            path="/dashboard"
            element={
              <Layout
                addEventCallbackRef={addEventCallback}
                dateChangedCallback={dateChangedCallback}
                userViewClickedCallback={userViewClicked}
                settingsViewClickedCallback={settingsViewClicked}
                subViewSessionFlag={subViewSessionFlag}
                mainView={
                  mainViewCategoryFlag ? (
                    <CategoryView
                      categoryID={selectedCategoryID!}
                      categoryTitle={selectedCategoryTitle!}
                      categoryColor={selectedCategoryColor!}
                      bubbleClickedCallback={bubbleClickedCallback}
                    />
                  ) : mainViewSessionFlag ? (
                    <StudyBubbleView
                      categoryColor={categoryColors[0]}
                      studyBubbleID={selectedBubbleID!}
                    />
                  ) : mainViewUserFlag ? (
                    <UserView logoutCallback={logoutCallback} />
                  ) : mainViewSettingsFlag ? (
                    <div></div>
                  ) : (
                    // <SettingsView categories={categories} />
                    <DashboardView
                      categories={categories}
                      categoryClicked={categoryClickedCallback}
                    />
                  )
                }
                subView={
                  addEvent ? (
                    <AddEvent
                      callbackRefresh={subViewCloseCallback}
                      date={Date().toString()}
                      categoryColors={categoryColors}
                    ></AddEvent>
                  ) : subViewBubbleFlag ? (
                    <TasklistView
                      studybubbleID={selectedBubbleID!}
                      isAvailable={
                        new Date(selectedBubbleStartDate!) < new Date()
                      }
                      closeCallback={subViewCloseCallback}
                      enterSessionCallback={enterSessionClickedCallback}
                    ></TasklistView>
                  ) : subViewSessionFlag ? (
                    <EventSubview
                      studyBubbleID={selectedBubbleID!}
                      exitSessionCallback={exitSessionCallback}
                    ></EventSubview>
                  ) : (
                    <UpcomingEvents
                      startDate={selectedDate!.toString()}
                      bubbleClickedCallback={bubbleClickedCallback}
                    ></UpcomingEvents>
                  )
                }
                date={Date.now()}
              ></Layout>
            }
          />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
