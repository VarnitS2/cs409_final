# CS 409 Final Project - Study Bubble

Eesha Moona - emoona2 \
Varnit Sinha - varnits2

## Application Setup

To run the application, you must first clone this repository to your local machine and install the required dependencies via `npm`.

Run the following commands in the **root** directory:

> To install backend dependencies
```
$ cd api/ && npm i
```

> To install frontend dependencies
```
$ cd client/ && npm i
```

## Run the Application

Run the following commands in the **root** directory:

> To start the backend
```
$ cd api/ && npm start
```

> Run in a separate terminal window to start the frontend
```
$ cd client/ && npm start
```

Open [http://localhost:3000](http://localhost:3000) to view the application in the browser.
